FROM python:3.10-alpine

COPY . /usr/src/healthsupbot

WORKDIR /usr/src/healthsupbot

RUN apk update \
    && python3 -m pip install --no-cache-dir --no-warn-script-location --upgrade pip \
    && python3 -m pip install --no-cache-dir --no-warn-script-location -r requirements.txt

CMD ["python3", "main.py"]
